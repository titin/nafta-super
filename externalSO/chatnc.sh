#!/bin/bash

if [ $# -lt 2 ]; then
	echo "debe pasar MODO_DE_EJECUCION y NOMBRE";
	exit 1;
fi

if [ $1 == "s" ]; then
	while [ true ]; do
		read -p "$2 - $(date): " mensaje
		echo "mensaje de $2 dice: $mensaje"
	done | nc -k -l -v 127.0.0.1 1234
fi

if [ $1 == "c" ]; then
	while [ true ]; do
		read -p "$2 a las $(date) dijo: " mensaje
		echo "mensaje de $2 dice: $mensaje"
	done | nc -v 127.0.0.1 1234
fi