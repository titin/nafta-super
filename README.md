## Primer entregable SO_2016
Implementacion de una shell en bash. 
Repositorio git: gitlab.com/titin/nafta-super

## Implementaciones y aclaraciones

> Se aceptan/interpretan las variables de entorno (como por ejemplo $HOME, $OLDPWD, etc) como parametro de los comandos, no se implementan pipes.
La variable $RUTA contiene los directorios que se encuentran en la variable de entorno $PATH. (Hay un comentario con los directorios de ejemplo del entregable).

### ls:
Tiene el mismo comportamiento que un ls de bash, a diferencia que cuando se ejecuta un ls en bash, este muestra los resultados en formato de columnas, mientras que el ls que se debe implementar deberá mostrar los resultados en una única columna (un resultado bajo el otro).

Deberá ser capaz de recibir el parámetro `-l`: El resultado de la ejecución de este comando con el parámetro -l devolverá un listado similar al de un ls -l tradicional, con la salvedad de que los permisos serán mostrados utilizando notación octal.

### sl:
Devuelve el mismo resultado que un ls, pero en orden inverso (la primer línea la mostrará última).

### cat: 
Tiene el mismo comportamiento que un cat de bash.

### tac: 
Devuelve el mismo resultado que un cat, pero en orden inverso (la primer línea la mostrará última).

### prompt:
Cambia el prompt de la shell. Recibe un string como parámetro:
* `largo`: el prompt se convierte en `@YoSoy-nombre_usuario>`
* `uid`: el prompt se convierte en `@uid_usuario>`
* `default`: toma la forma por defecto.

### quiensoy: 
Imprime información acerca de quien esta logueado. Recibe SOLO un string como parámetro, debiendo chequearse:
* Si la cantidad de parámetros no es exactamente uno imprime “Cantidad de argumentos incorrecta. Solo es posible recibir uno solo.”
* Recibe `+h`: imprime `Yo soy nombre_usuario y estoy en la maquina “nombre_host”`.
* Recibe `+inos`: imprime `Yo soy nombre_usuario y tengo UID=uid_usuario`.
* Caso contrario imprime `Yo soy nombre_usuario`

### pwd: 
Tiene el mismo comportamiento que un `pwd` de bash.

### mkdir: 
Crea un directorio dentro del FileSystem. Como **primer parámetro** siempre debe recibir el **path del directorio a crear** y a continuación cualquier otro parámetro soportado por el comando mkdir de bash (cada parametro debe ir separado, por ejemplo, `-p -v`. También tendrá que tener en cuenta lo siguiente: 
* Si el usuario que lo ejecuta no tiene permiso para escribir en la estructura del FileSystem, deberá devolver el mensaje de error ”No tenés permiso!”, sin mostrar otro error en la pantalla.
* Si al momento de realizar la operación hay algun error se debera visualizar
en pantalla ”Error!, Directorio/s no creado/s” y nada más.
> No se aceptan nombres de carpetas con espacios!!

### serverNC: 
que implementa el servidor del ejercicio uno de la primer práctica.

### clientNC: 
que implemente el cliente del ejercicio uno de la primer práctica.

### scanner: 
que implemente el ejercicio tres de la primer práctica pero recibiendo la lista hosts por parámetros.