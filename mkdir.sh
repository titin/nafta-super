function mkcar(){
	nuevo=" "; arg=" "; flag_p=" "
	dir_padre=" "

	function parse(){
		if [[ $# -eq 0 ]]; then 
			echo "Se debe pasar un directorio a crear y los parametros"
			continue
		fi
		if [[ ${1:0:1} == "-" ]]; then
			echo "falto el nombre o ruta del directorio a crear"
			continue
		fi
		nuevo=$1
		if [[ -d $nuevo ]]; then
			echo "Ya existe el directorio"
			continue #exit 1
		fi
		for i in ${*:2}; do 
			if [[ $i == "-p" ]]; then
				flag_p=0
			else
				arg+=$i
			fi
		done
	}

	function crear_with_p(){
		exito=0
		dir_padre=$(dirname $1)

		if [[ ! -d $dir_padre ]]; then
			exito=[ crear_with_p $dir_padre ]
		fi
		if [[ $exito -eq 0 && -w $dir_padre ]]; then
			mkdir $1 $arg 2> /dev/null ; return $?
		else
			return 2
		fi
	}

	parse $*
	crear_with_p $nuevo
	if [[ $? -ne 0 ]]; then 
		echo "hubo un error"
	fi
}

mkcar $*