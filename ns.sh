#!/bin/bash

####### VARIABLES #######

BASE=$(dirname $0)
if [[ $BASE == "." || $BASE == " " ]]; then BASE=$(pwd); fi
SO="externalSO"

source "$BASE/colors.sh" # colores para el prompt

prompt_text=$(whoami)
prompt="$( echo -e "${BIPur}@${BICya} $prompt_text ${Gre}>>${RCol}")"
comando=" "
parametros=" "
argc=" "
RUTA=" "

####### fin VARIABLES #######

function ini_ruta(){
	RUTA="$( echo $PATH | tr ":" "\n" | tac | tr "\n" ";")$BASE/$SO"
	#RUTA="$( echo "/usr/local/bin:/usr/bin:/bin" | tr ":" "\n" | tac | tr "\n" ";")./$SO"
}

function welcome(){
	clear
	echo -e "\n ${BBlu}Entrando a NS Shell... ${RCol} \n"
}

function parse_arg(){
# Parsear arguemnetos con eval para interpretar las $variables_de_entorno
# si: <eval $algo> no esta definido, se devuelve string '$algo'

	comando=${line[0]}
	#parametros=(${line[@]:1}) # parametros sin evaluar
	parametros=()
	cont=0
	for p in ${line[@]:1}; do 
		par=${p//"\""}
		evaluado=$(eval echo $par)
		if [ ${#evaluado} -eq 0 ]; then
			evaluado=$p
		fi
		parametros[$cont]=$evaluado
		let cont++
	
	done
	argc=(${#parametros[@]})
}

function ls(){
	arg="-"; ruta=" "; flag_l=1; archivo=" "; actual=" "
	
	function parse(){
	# parsea los parametros y rutas guardando los parametros en la variable 'arg',
	# las rutas en 'ruta', y si hay un -l, setea el 'flag_l'

		for p in $*; do
			primer_caracter=${p:0:1}
			arg_actual=${p:1}
			if [ $primer_caracter == "-" ]; then
				if [[ $arg_actual == *l* ]]; then
					flag_l=0
				fi
				arg+=${arg_actual//l/} # saca las "l"
			else
				ruta="$p"
			fi
		done
	}

	function condicion(){
	# contempla los casos vacios para los parametros y la ruta
	# corta la ejecucion si existe un parametro l y R al mismo tiempo

		if [[ $arg == "-" ]]; then arg=" "; fi
		if [[ $ruta == " " ]]; then ruta="."; fi
		if [[ $arg == *R* && $flag_l == 0 ]]; then
			echo "No se permite los parametros <<l>> y <<R>> juntos"
			continue;
		fi		
	}

	function pre_posicion(){
	# esta funcion determina si se debe cambiar la posicion o no, dependiendo
	# de si se hace un ls -l archivo.ext ó ls -l /ruta/absoluta o ls -l ruta_relativa
	
		if [[ -d $ruta ]]; then
			actual=$(pwd)
			if [[ ${ruta:0:1} != "/" && ${ruta:0:1} != "." ]]; then
				ruta="$(pwd)/$ruta"
			fi
			cd $ruta
		else
			archivo=$ruta
		fi
	}

	function pos_posicion(){
		if [[ -d $ruta ]]; then
			cd $actual
		fi
	}

	function ejecucion_l(){
	# ls -l que se requerido en la entrega
	# while read name, permite tomar nombre con espacios

		pre_posicion
		command ls $arg $archivo | while read name; do
			string=$(command ls "$name" -ld $arg)
			echo ${string:0:1} $(stat "$name" -c "%a") ${string:10}
		done
		pos_posicion
	}

	function ejecucion_normal(){
		command ls -1 $arg $ruta
	}

	function ejecucion(){
	# Segun flag_l (indica si se recibio o no el parametro -l):
	# => 0, se recibio "-l", entonces, ejecuta la implementacion de la entrega
	# => 1, no se recibio "-l", ejecuta ls normal listando uno debajo de otro.

		if [[ $flag_l -eq 0 ]]; then 
			ejecucion_l
		else
			ejecucion_normal
		fi
	}

	parse $*
	condicion
	ejecucion
}

function exit(){
# funcion de salida de la shell
	
	echo -e "\n ${Red}Saliendo de NS... ${RCol} \n"
	command exit
}

function sl(){
	ls -1 -r $*
}

function cat(){
	command cat $*
}

function tac(){
	command tac $*
}

function pwd(){
	command pwd $*
}

function mkdir(){
# se deben pasar los parametros separados -p -v (no -pv)
# primero la ruta y despues lo parametros

	nuevo=" "; arg=" "; flag_p=1; flag_v=1

	function parse(){
		if [[ $# -eq 0 ]]; then 
			echo "Se debe pasar un directorio a crear y los parametros"
			continue
		fi
		if [[ ${1:0:1} == "-" ]]; then
			echo "falto el nombre o ruta del directorio a crear"
			continue
		fi
		nuevo=$1
		if [[ -d $nuevo ]]; then
			echo "Ya existe el directorio"
			continue #exit 1
		fi
		for i in ${*:2}; do 
			if [[ $i == "-p" ]]; then flag_p=0
			elif [[ $i == "-v" ]]; then flag_v=0; arg+=$i
			else arg+=" $i"
			fi
		done
	}

	function crear_recursivo(){
	# error = 2 => error de permisos
	# error = 1 => error status mkidr ($?)
	# exito = 0 => exito status mkidr ($?)

		exito=0
		dir_padre=$(dirname $1)

		if [[ ! -d $dir_padre ]]; then
			exito=[ crear_recursivo $dir_padre ]
		fi
		if [[ $exito -eq 1 ]]; then # error mkdir
			return 1
		fi
		if [[ -w $dir_padre ]]; then
			command mkdir $1 $arg 2> /tmp/m_verbose ; return $?
		else
			return 2 # error de permisos
		fi
	}

	parse $*

	if [[ -d $(dirname $nuevo) ]]; then
		crear_recursivo $nuevo
	elif [[ $flag_p -eq 0 ]]; then
		crear_recursivo $nuevo
	else
		echo "Error directorio: $(dirname $nuevo) no esta creado"
	fi
	if [[ $flag_v -eq 0 ]]; then cat /tmp/m_verbose; fi
	if [[ $? -eq 1 ]]; then echo "Error al crear las carpetas"
	elif [[ $? -eq 2 ]]; then echo "No tenes permisos"; fi
}

function quiensoy(){
	if [[ $# -gt 1 ]]; then
		echo -e "${On_IRed}Error:${RCol}"
		echo "Cantidad de argumentos incorrecta."
		echo "Se espera 1 (solo) argumento."

	elif [[ $1 == "+h" ]]; then
		echo -n -e "Yo soy ${BRed}$(whoami)${RCol} y "
		echo -e "estoy en la maquina ${BRed}$(hostname)${RCol}"

	elif [[ $1 == "+inos" ]]; then
		echo -n -e "Yo soy ${BRed}$(whoami)${RCol} y "
		echo -e "tengo ${BRed}UID=$(id -u)${RCol}"

	else
		echo -e "Yo soy ${BRed}$(whoami)${RCol}"

	fi
}

function refresh_prompt(){
	prompt_text=$1
	prompt="$( echo -e "${BIPur}@${BICya} $prompt_text ${Gre}>>${RCol}" )"
}

function prompt(){
	if [ $# -gt 1 ]; then
		echo -e "${On_IRed}Error:${RCol}"
		echo -e "Demasiados argumentos...\n" 

	elif [ $# -eq 0 ]; then
		echo -e "${On_IRed}Error:${RCol}"
		echo -e "Faltan parametros \n"

	elif [ $1 = "largo" ]; then
		refresh_prompt "YoSoy-$(whoami)"

	elif [ $1 = "uid" ]; then
		refresh_prompt $(id -u)

	elif [ $1 = "default" ]; then
		refresh_prompt $(whoami)

	else
		echo -e "${On_IRed}Error:${RCol}"
		echo -e "Parametro no valido \n"
	fi
}

function control_c(){
	continue
	echo " "
}

trap control_c SIGINT
########################### loop principal ###################################
welcome; ini_ruta
while [ true ]; do
	# Imprimir prompt y leer lo ingresado por usuario
	read -p "$prompt " -a line

	if [[ $? -eq 1 ]]; then command exit; fi # "trapeo" el ctrl D

	# Validaciones
	# Obtener comandos y sus parametros
	if [[ -z $line ]]; then continue; fi
	
	parse_arg
	if [[ $comando == "exit" ]]; then exit; fi

	existe=$( type -t $comando )
	type_exit_status=$?

	# Ejecutar built-in de esta shell y esperar si es necesario
	# O ejecutar built-in bash
	# o ejecutar comando externo con el criterio descripto mas abajo

	if [[ $comando == "prompt" ]]; then 
	# si se ejecuta el background no se reflejan los cambios
		$comando ${parametros[@]}
		continue
	fi

	if [[ $existe == "function" ]]; then
		$comando ${parametros[@]} &
		wait $!

	elif [[ $existe == "builtin" ]]; then
		echo -e "${BIGre}BUILTIN Bash ${RCol}"
		command $comando ${parametros[@]}

	else
		echo -e "${BIGre}Buscando en RUTA ${RCol}"
		existe=1
		buscar=1
		for directorio in $(echo $RUTA | tr ";" "\n" | tac ); do
			if [[ -x "$directorio/$comando" ]]; then
				buscar=0; existe="$directorio/$comando"
			fi
		done
		if [[ $buscar -eq 0 ]]; then 
			$existe ${parametros[@]}
		else
			echo -e " ${On_IRed} El comando << $comando >> no existe... ${RCol}"
		fi
	fi

done
