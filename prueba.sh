function lss(){
	arg="-"
	ruta=" "
	flag_l=1
	archivo=" "
	
	function parse(){
	# parsea los parametros y rutas, destacando el arg -l
	# guardando los parametros en la variable arg
	# las rutas en "ruta", y si hay un -l, setea el flag_l

		for p in $*; do
			if [ ${p:0:1} == "-" ]; then
				if [[ $p == *l* ]]; then
					flag_l=0
				fi
				arg_actual=${p:1}
				arg+=${arg_actual//l/} # saca las "l"
			else
				ruta="$p"
			fi
		done
	}

	function condicion(){
	# contempla los casos vacios y corta la ejecucion
	# si existe un parametro l y R al mismo tiempo

		if [[ $arg == "-" ]]; then arg=" "; fi
		if [[ $ruta == " " ]]; then ruta="."; fi
		if [[ $arg == *R* && $flag_l == 0 ]]; then
			echo "No se permite los parametros <<l>> y <<R>> juntos"
			continue;
		fi		
		echo " argumentos = $arg | ruta = $ruta "
	}

	function pre_posicion(){
		if [[ -d $ruta ]]; then
			actual=$(pwd)
			cd $ruta
		else
			archivo=$ruta
		fi
	}

	function pos_posicion(){
		if [[ -d $ruta ]]; then
			cd $actual
		fi
	}

	function ejecucion_l(){
	# Dependiendo de flag_l ejecuta el ls original
	# sino ejecuta una pseudo implementaicon del ls -l
		pre_posicion
		ls $arg $archivo | while read name; do
			#while read name, permite tomar nombre con espacios
			string=$(ls "$name" -ld $arg)
			echo ${string:0:1} $(stat "$name" -c "%a") ${string:10}
		done
		pos_posicion
	}

	function ejecucion_normal(){
		echo -e "${Cya}"
		ls -1 $arg $ruta
		echo -e "${RCol}"
	}

	function ejecucion(){
		if [[ $flag_l -eq 0 ]]; then 
			ejecucion_l
		else
			ejecucion_normal
		fi
	}

	parse $*
	condicion
	ejecucion
}

lss $*

function ls_sed(){
	ls -l / | sed -e "s/rwx/7/g"
			 -e "s/rw-/6/g" -e "s/r-x/5/g" 
			 -e "s/r--/4/g" -e "s/-wx/3/g" 
			 -e "s/-w-/2/g" -e "s/--x/1/g" 
			 -e "s/---/0/g"

}

function crear_with_p(){
                exito=0
                dir_padre=$(dirname $1)

                if [[ ! -d $dir_padre ]]; then 
                        exito=[ crear_with_p $dir_padre ]
                fi
                if [[ $exito -eq 0 && -w $dir_padre ]]; then
                        mkdir $1 2> /dev/null; exito=$?
                        if [[ $exito -ne 0 ]]; then
                                echo "hubo un error con $1"
                        fi
                        return $exito
                else
                        return 2
                fi

        }

        crear_with_p "desde/donde\ est/estoy/parado"



